import React from "react";
import { useState } from "react";
import CalButton from "../calculator-app/CalButton";
function Calculator() {
    const [exp, setExp] = useState('');
    const [elqClicked, setEqlClicked] = useState(false);
    console.log("exp",exp)
    let array = ['0','1','2','3','4','5','6','7','8','9','+']
    const onButtonClick = (number) => {
        if (elqClicked) {
            setExp(number);
            setEqlClicked(false);
            return
        }
        if (!(number === '+' && exp[exp.length - 1] === '+')) {
            setExp(exp + number)
        }
    }
    const calculate = () => {
        setEqlClicked(true);
        let ans = 0;
        let nums = [];
        nums = exp.split('+');
        for (let n of nums) {
            ans += (n === '' ? 0 : parseInt(n));
        }
        setExp(ans.toString());
    }
    return (
        <>
            <center style={{ marginTop: "20%", hight: "20%" }}>
                <input style={{ width: "5%" }}  value={exp}  onChange={()=>{}}/><br />
                <div style={{ hight: "90%" }}>
                {
                    array.map((data,index)=>{
                    
                        return(
                            <CalButton key={index} display={data} onClick={onButtonClick}/>
                        )
                    })
                }
                <CalButton display='=' onClick={calculate}/>
                </div>
            </center>
        </>
    );
}
export default Calculator;
