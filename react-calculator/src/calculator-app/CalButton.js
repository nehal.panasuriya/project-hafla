import React from "react";
function CalButton({display,onClick}) {
    return (
        <button onClick={() => { onClick(display) }}>{display}</button>
    );
  }
export default CalButton;